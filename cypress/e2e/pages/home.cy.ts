describe('Home Page', () => {
    beforeEach(() => {
        cy.visit('http://localhost:3000/');
    });

    it('should display hero section', () => {
        cy.get('[data-testid="hero"]').should('exist');
    });

    it('should display video section', () => {
        cy.get('[data-testid="video"]').should('exist');
    });

    it('should display "Who we are" section', () => {
        cy.get('[data-testid="who-we-are"]').should('exist');
    });

    it('should display carousel in "Who we are" section', () => {
        cy.get('[data-testid="carousel"]').should('exist');
    });

    it('should display "Start saving today" section', () => {
        cy.get('[data-testid="start-saving"]').should('exist');
    });

    it('should display stats section', () => {
        cy.get('[data-testid="team-stats"]').should('exist');
    });
});
