import type { Config } from "tailwindcss";

const config: Config = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
    "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
  ],
  theme: {
    extend: {
      fontSize: {
        'hero': 'clamp(1.75rem, 3.6vw, 4.375rem)',
      },
      lineHeight: {
        'hero-lh': '4.125rem', // Custom line-height
      },
      backgroundImage: {
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
      },
      colors: {
        'vq-blue': '#2578FF',
        'vq-light-blue': '#CCF7FC',
        'vq-black': '#05192d',
      },
    },
  },
  plugins: [],
};
export default config;
