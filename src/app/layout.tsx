import type {Metadata} from "next";
import {Inter, Poppins} from "next/font/google";
import "./globals.css";

const poppins = Poppins({subsets: ["latin"], weight: ["600", "500"]});

export const metadata: Metadata = {
    title: "Top Secret Veeqo Project",
    category: 'technology',
    bookmarks: ['https://veeqo.com'],
    description: "Built by Daniel",
    generator: 'Daniel.js',
    applicationName: 'Daniel.js',
    referrer: 'origin-when-cross-origin',
    keywords: ['Veeqo'],
    creator: 'Daniel Yeoman',
    formatDetection: {
        email: false,
        address: false,
        telephone: false,
    },
    alternates: {
        canonical: 'https://www.veeqo.com',
        languages: {
            'en-GB': 'https://www.veeqo.com/gb'
        }
    },
    robots: {
        index: true,
        follow: true,
        nocache: false,
        googleBot: {
            index: true,
            follow: true,
            noimageindex: false,
            'max-video-preview': 0,
            'max-image-preview': 'large',
            'max-snippet': -1,
        },
    },
}

export default function RootLayout({
                                       children,
                                   }: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
        <body className={poppins.className}>
        <div className="w-full overflow-x-hidden bg-vq-black">
            <div className="max-w-screen-2xl mx-auto px-4 sm:px-6 lg:px-8 pt-8">
                {children}
            </div>
        </div>
        </body>
        </html>
    );
}
