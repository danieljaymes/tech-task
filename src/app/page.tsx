import Hero from "@/components/Hero";
import Video from "@/components/Video";
import Tag from "@/components/Tag";
import Carousel from "@/components/Carousel";
import Content from "@/components/Content";
import Button from "@/components/Button";
import StatsContent from "@/components/StatsContent";
import type { Metadata } from 'next'

export const metadata: Metadata = {
    title: 'Veeqo Project',
    description: 'Veeqo Top Secret Project',
}

export default function Home() {

    const heroText = (
        <>
            Own your <span className={'text-vq-blue'}>future</span> simple and for <span
            className={'text-vq-blue'}>free</span>
        </>
    );

    return (
        <>
            <main role="main" className="flex min-h-screen flex-col items-center justify-between">
                <Hero
                    logoSrc={'/assets/icons/icon-hero.png'}
                    logoSrcAlt={'Veeqo Project Logo'}
                    text={heroText}
                    structuredData={{
                        "@context": "https://schema.org",
                        "@type": "Organization",
                        "url": "https://example.com",
                        "logo": "https://example.com/logo.png",
                        "description": "Description of the organization",
                    }}
                />
                <Video
                    src="/assets/hero-video.mp4"
                    title="This is a Veeqo Video"
                    description="This is about Veeqo."
                    poster="/assets/hero-video-poster-image.png"
                    uploadDate="2024-04-04T00:00:00Z"
                    duration="PT0M19S"
                />
                <section className="mt-20" aria-labelledby="who-we-are-heading">
                    <div className="max-w-screen-lg mb-32 mx-auto">
                        <div className="flex items-start space-x-6">
                            <div className="text-left flex-1 mt-2">
                                <h2 id="who-we-are-heading" className="sr-only">Who we are</h2>
                                <Tag text={'Who we are'} textColour={'text-black'} bgColour={'bg-vq-blue'}
                                     cornerRadius={'rounded-2xl'}/>
                            </div>

                            <div className="flex-1">
                                <p className="text-white leading-10 font-medium text-xl">
                                    Our one of a kind integration lorem ipsum dolor sit amet, consectetur adipiscing
                                    elit,
                                    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                                </p>
                                <p className="text-white mt-8 leading-10 font-medium text-xl">
                                    As part of the Amazon family, <span className={'text-vq-blue'}>Veeqo </span>
                                    provides trusted data security and Amazon account
                                    protection
                                    from late deliveries and negative feedback, if you ship on time.
                                </p>
                            </div>
                        </div>
                    </div>
                    <Carousel images={[
                        {src: '/assets/intro-image-1.png', width: 320, height: 320},
                        {src: '/assets/intro-image-2.png', width: 320, height: 320},
                        {src: '/assets/intro-image-3.png', width: 320, height: 320},
                        {src: '/assets/intro-image-4.png', width: 320, height: 320},
                    ]}/>
                </section>
                <section className="bg-white mt-20 -mx-8" aria-labelledby="start-saving-heading">
                    <Content
                        tag={<Tag bgColour={'bg-vq-black'} cornerRadius={'rounded-2xl'} text={'Start saving today'}
                                  textColour={'text-white'}/>}
                        button={<Button href={'https://www.veeqo.com'}>Start Shipping</Button>}
                        headerText="See how much you could save"
                        paragraphText="Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vulputate semper sem nec sodales. Vestibulum vitae vulputate metus."
                        imageSrc="/assets/feature-image-1.png"
                        imagePosition="left"
                        structuredData={{
                            "@context": "http://schema.org",
                            "@type": "Article",
                            "mainEntityOfPage": {
                                "@type": "WebPage",
                                "@id": "https://example.com/content"
                            },
                            "headline": "Article headline",
                            "image": [
                                "https://example.com/image.jpg"
                            ],
                            "datePublished": "2024-04-04T12:34:56Z",
                            "dateModified": "2024-04-05T12:34:56Z",
                            "author": {
                                "@type": "Person",
                                "name": "John Doe"
                            },
                            "publisher": {
                                "@type": "Organization",
                                "name": "OpenAI",
                                "logo": {
                                    "@type": "ImageObject",
                                    "url": "https://example.com/logo.jpg"
                                }
                            },
                            "description": "This is a sample article description."
                        }}
                    />
                </section>
                <section data-testid="who-we-are" aria-labelledby="team-stats-heading">
                    <StatsContent
                        tag={<Tag bgColour={'bg-vq-light-blue'} cornerRadius={'rounded-xl'} text={'We’re a team of'}
                                  textColour={'text-black'}/>}
                        headerText={"1,000 People"}
                        statImage={'/assets/stats-image.png'}
                        structuredData={{
                            "@context": "http://schema.org",
                            "@type": "WebPage",
                            "name": "Stats Content Page",
                            "description": "This page contains statistics about various aspects.",
                            "url": "https://example.com/stats-content",
                            "publisher": {
                                "@type": "Organization",
                                "name": "Example Publisher",
                                "logo": {
                                    "@type": "ImageObject",
                                    "url": "https://example.com/logo.png"
                                }
                            }
                        }}
                        stats={
                            [
                                {
                                    imageSrc: '/assets/icons/marketing.png',
                                    imageAlt: 'Marketing Managers',
                                    statNumber: '500',
                                    statString: 'Marketing Managers'
                                },
                                {
                                    imageSrc: '/assets/icons/product-design.png',
                                    imageAlt: 'Product Designers',
                                    statNumber: '100',
                                    statString: 'Product Designers'
                                },
                                {
                                    imageSrc: '/assets/icons/frontend.png',
                                    imageAlt: 'Front-End Engineers',
                                    statNumber: '320',
                                    statString: 'Front-End Engineers'
                                },
                                {
                                    imageSrc: '/assets/icons/backend.png',
                                    imageAlt: 'Back-End Engineer',
                                    statNumber: '1',
                                    statString: 'Back-End Engineer'
                                },
                                {
                                    imageSrc: '/assets/icons/support.png',
                                    imageAlt: 'Customer Support Team',
                                    statNumber: '250',
                                    statString: 'Customer Support Team'
                                },
                                {
                                    imageSrc: '/assets/icons/finance.png',
                                    imageAlt: 'Finance Managers',
                                    statNumber: '20',
                                    statString: 'Finance Managers'
                                },
                            ]
                        }
                    />
                </section>
                <section data-testid="start-saving" className="bg-white mt-20 -mx-8" aria-labelledby="start-saving-heading">
                    <Content
                        tag={<Tag bgColour={'bg-vq-black'} cornerRadius={'rounded-xl'} text={'Start saving today'}
                                  textColour={'text-white'}/>}
                        button={<Button href={''}>Start Shipping</Button>}
                        headerText="See how much you could save"
                        paragraphText="Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aenean vulputate semper sem nec sodales. Vestibulum vitae vulputate metus."
                        imageSrc="/assets/feature-image-2.png"
                        imagePosition="right"
                        structuredData={{
                            "@context": "http://schema.org",
                            "@type": "Article",
                            "mainEntityOfPage": {
                                "@type": "WebPage",
                                "@id": "https://example.com/content"
                            },
                            "headline": "Article headline",
                            "image": [
                                "https://example.com/image.jpg"
                            ],
                            "datePublished": "2024-04-04T12:34:56Z",
                            "dateModified": "2024-04-05T12:34:56Z",
                            "author": {
                                "@type": "Person",
                                "name": "John Doe"
                            },
                            "publisher": {
                                "@type": "Organization",
                                "name": "OpenAI",
                                "logo": {
                                    "@type": "ImageObject",
                                    "url": "https://example.com/logo.jpg"
                                }
                            },
                            "description": "This is a sample article description."
                        }}
                    />
                </section>
            </main>
        </>
    );
}
