'use client'
import React, {useRef, useState} from 'react';
import Image from 'next/legacy/image';
import Script from "next/script";

type VideoProps = {
    src: string;
    title: string;
    description: string;
    poster?: string;
    uploadDate: string;
    duration: string;
}

/**
 * Video component for displaying a video player and its metadata.
 *
 * @component
 * @param {object} VideoProps - The properties passed to the Video component.
 * @param {string} VideoProps.src - The URL of the video source.
 * @param {string} VideoProps.title - The title of the video.
 * @param {string} VideoProps.description - The description of the video.
 * @param {string} VideoProps.poster - The URL of the video poster image.
 * @param {string} VideoProps.uploadDate - The upload date of the video.
 * @param {number} VideoProps.duration - The duration of the video.
 * @returns {JSX.Element} - The Video component.
 *
 */
const Video: React.FC<VideoProps> = ({
                                               src,
                                               title,
                                               description,
                                               poster,
                                               uploadDate,
                                               duration,
                                           }) => {
    const [play, setPlay] = useState(false);
    const videoRef = useRef<HTMLVideoElement>(null);

    const structuredData = {
        "@context": "http://schema.org",
        "@type": "VideoObject",
        "name": title,
        "description": description,
        "thumbnailUrl": poster,
        "uploadDate": uploadDate,
        "duration": duration,
        "contentUrl": src,
    };

    const handlePlay = () => {
        if (videoRef.current) {
            if (play) {
                videoRef.current.pause();
                setPlay(false);
            } else {
                videoRef.current.play();
                setPlay(true);
            }
        }
    };

    return (
        <>
            <div className="relative video-container">
                <video
                    data-testid="video"
                    ref={videoRef}
                    style={{ aspectRatio: '16 / 9' }}
                    src={src}
                    title={title}
                    controls={play}
                    poster={poster}
                    aria-describedby="video-description"
                >
                    <track kind="captions" />
                </video>
                {!play && (
                    <button
                        className="absolute top-1/2 left-1/2 transform -translate-x-1/2 -translate-y-1/2 cursor-pointer"
                        onClick={handlePlay}
                    >
                        <Image src={'/assets/icons/play-button.png'} alt="Play Video" width={75} height={75} objectFit="contain" />
                    </button>
                )}
                <div id="video-description" className="hidden">
                    {description}
                </div>
                {structuredData && (
                    <Script id="hero-ld" type="application/ld+json">{JSON.stringify(structuredData)}</Script>
                )}
            </div>
        </>
    );
};

export default Video;