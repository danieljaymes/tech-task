import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/jest-globals';
import { describe, expect } from '@jest/globals';
import Video from './Video';

describe('Video Component', () => {
    const mockProps = {
        src: '/videos/test-video.mp4',
        title: 'Test Video',
        description: 'This is a test video',
        poster: '/images/test-poster.jpg',
        uploadDate: '2022-04-04',
        duration: 'PT1M30S'
    };

    it('renders with no issues or errors', () => {
        render(<Video {...mockProps} />);
    });

    it('displays correct video title', () => {
        const { getByTitle } = render(<Video {...mockProps} />);
        expect(getByTitle(mockProps.title)).toBeInTheDocument();
    });
});
