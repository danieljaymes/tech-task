import React from 'react';
import Image from 'next/legacy/image';
import StatItem from "@/components/StatItem";
import Script from "next/script";

type ContentProps = {
    tag: JSX.Element;
    headerText: string;
    statImage: string;
    stats: { imageSrc: string; imageAlt: string; statNumber: number | string; statString: string }[];
    structuredData?: Record<string, any>;
};

/**
 * Represents a component displaying statistics content.
 * @param {React.FC<ContentProps>} StatsContent - The component function.
 * @param {string} tag - The tag of the content.
 * @param {object} structuredData - The structured data for SEO.
 * @param {string} headerText - The header text of the content.
 * @param {string} statImage - The image for the statistics.
 * @param {Array<object>} stats - The array of statistics to display.
 * @returns {ReactElement} - The rendered React element of the stats content.
 */
const StatsContent: React.FC<ContentProps> = ({tag, structuredData, headerText, statImage, stats}) => {
    return (
        <article>
            <div data-testid="team-stats" className="flex flex-row p-8">
                <div className="pt-36">
                    {tag}
                    <h2 className="text-[10rem] leading-[8.75rem] pt-8 text-white font-bold my-4">{headerText}</h2>
                </div>
                <div>
                    <Image src={statImage} alt={headerText} width={450} height={400}/>
                </div>
            </div>
            <div className="w-full flex justify-end pr-36 mt-8">
                <div className="grid grid-cols-2 gap-x-32 gap-y-14">
                    {stats.map((stat, index) => (
                        <StatItem key={index} {...stat} />
                    ))}
                </div>
            </div>
            {structuredData && (
                <Script id="stats-ld" type="application/ld+json">{JSON.stringify(structuredData)}</Script>
            )}
        </article>
    );
};

export default StatsContent;
