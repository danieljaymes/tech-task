import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/jest-globals';
import { describe, expect } from '@jest/globals';
import StatsContent from './StatsContent';

describe('StatsContent Component', () => {
    const mockProps = {
        tag: <div>Sample Tag</div>,
        headerText: 'Sample Header',
        statImage: '/path/to/image.png',
        stats: [
            { imageSrc: '/path/to/image1.png',imageAlt: "test", statNumber: 10, statString: 'Sample Stat 1' },
            { imageSrc: '/path/to/image2.png',imageAlt: "test", statNumber: '20', statString: 'Sample Stat 2' }
        ],
        structuredData: {}
    };

    it('renders with no issues or errors', () => {
        render(<StatsContent {...mockProps} />);
    });

    it('displays correct header text', () => {
        const { getByText } = render(<StatsContent {...mockProps} />);
        expect(getByText(mockProps.headerText)).toBeInTheDocument();
    });

    it('renders correct number of stats', () => {
        const { getAllByTestId } = render(<StatsContent {...mockProps} />);
        expect(getAllByTestId('stat-item')).toHaveLength(mockProps.stats.length);
    });
});
