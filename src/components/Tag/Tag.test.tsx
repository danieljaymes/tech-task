import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/jest-globals';
import { describe, expect } from '@jest/globals';
import Tag from './Tag';

describe('Tag Component', () => {
    const mockProps = {
        text: 'Sample Tag',
        textColour: 'text-white',
        bgColour: 'bg-blue-500',
        cornerRadius: 'rounded-md'
    };

    it('renders with no issues or errors', () => {
        render(<Tag {...mockProps} />);
    });

    it('displays correct tag text', () => {
        const { getByText } = render(<Tag {...mockProps} />);
        expect(getByText(mockProps.text)).toBeInTheDocument();
    });
});
