import React from 'react';

type TagProps = {
    text: string;
    textColour: string;
    bgColour: string;
    cornerRadius: string;
}

/**
 * Represents a Tag component in React.
 *
 * @param {Object} props - The component props.
 * @param {string} props.text - The text content of the tag.
 * @param {string} props.textColour - The colour of the tag text.
 * @param {string} props.bgColour - The background colour of the tag.
 * @param {string} props.cornerRadius - The corner radius of the tag.
 * @returns {ReactElement} A div element representing the tag.
 */
const Tag: React.FC<TagProps> = ({ text, textColour, bgColour, cornerRadius }) => {
    return (
        <div role="note" className={`inline-block px-3 py-1 ${textColour} ${bgColour} ${cornerRadius}`}>
            {text}
        </div>
    );
};

export default Tag;