import Image from 'next/image';

type StatItemProps = {
    imageSrc: string;
    imageAlt: string;
    statNumber: number | string;
    statString: string;
};

/**
 * Render a statistic item component.
 *
 * @component
 * @param {object} props - The props object.
 * @param {string} props.imageSrc - The source URL of the image.
 * @param {number} props.statNumber - The numerical value of the statistic.
 * @param {string} props.statString - The description string of the statistic.
 * @returns {ReactElement} The rendered StatItem component.
 */
const StatItem: React.FC<StatItemProps> = ({imageSrc, imageAlt, statNumber, statString}) => {
    return (
        <figure data-testid="stat-item" className="text-left w-40 flex flex-col items-start space-y-4">
            <Image src={imageSrc} alt={imageAlt} width={50} height={50} className="rounded-full"/>
            <figcaption>
                <p><span className="font-bold text-white">{statNumber}</span> <span
                    className="text-gray-400">{statString}</span></p>
            </figcaption>
        </figure>
    );
};

export default StatItem;
