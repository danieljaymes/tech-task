import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/jest-globals';
import { describe, expect } from '@jest/globals';
import StatItem from './StatItem';

describe('StatItem Component', () => {
    const mockProps = {
        imageSrc: '/path/to/image.png',
        imageAlt: "test",
        statNumber: 10,
        statString: 'Sample Stat'
    };

    it('renders with no issues or errors', () => {
        render(<StatItem {...mockProps} />);
    });

    it('displays correct stat number', () => {
        const { getByText } = render(<StatItem {...mockProps} />);
        expect(getByText(mockProps.statNumber.toString())).toBeInTheDocument();
    });

    it('displays correct stat string', () => {
        const { getByText } = render(<StatItem {...mockProps} />);
        expect(getByText(mockProps.statString)).toBeInTheDocument();
    });
});
