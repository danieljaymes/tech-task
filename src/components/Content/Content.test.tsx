import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/jest-globals';
import { describe, expect } from '@jest/globals';
import Content from './Content';

describe('Content Component', () => {
    const mockProps = {
        tag: <div>Mock Tag</div>,
        button: <button>Mock Button</button>,
        headerText: 'Mock Header Text',
        paragraphText: 'Mock Paragraph Text',
        imageSrc: '/mock_image.jpg',
        imagePosition: 'left',
        structuredData: {
            "@context": "http://schema.org",
            "@type": "Article",
            "headline": "Mock Headline",
            "description": "Mock Description",
            "datePublished": "2024-04-04",
            "image": "mock_image.jpg"
        }
    };

    it('renders with no issues or errors', () => {
        render(<Content {...mockProps} />);
    });

    it('displays correct header and paragraph text', () => {
        const { getByText } = render(<Content {...mockProps} />);
        expect(getByText(mockProps.headerText)).toBeInTheDocument();
        expect(getByText(mockProps.paragraphText)).toBeInTheDocument();
    });

});
