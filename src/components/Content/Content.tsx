import React from 'react';
import Image from 'next/legacy/image';
import Script from "next/script";

type ContentProps = {
    tag: JSX.Element;
    button: JSX.Element;
    headerText: string;
    paragraphText: string;
    imageSrc: string;
    imagePosition: string;
    structuredData?: Record<string, any>;
}

/**
 * Represents a content component.
 *
 * @typedef {Object} ContentProps
 * @property {ReactNode} tag - The tag element to be displayed.
 * @property {ReactNode} button - The button element to be displayed.
 * @property {string} headerText - The header text to be displayed.
 * @property {string} paragraphText - The paragraph text to be displayed.
 * @property {string} imageSrc - The image source for the image to be displayed.
 * @property {string} imagePosition - The position of the image ('left' or 'right').
 * @property {Object} structuredData - The structured data object.
 */
const Content: React.FC<ContentProps> = ({ tag, button, headerText, paragraphText, imageSrc, imagePosition, structuredData }) => {

    const flexDirection = imagePosition === 'left' ? 'flex-row' : 'flex-row-reverse';
    const justifyDirection = imagePosition === 'left' ? 'justify-end' : 'justify-start';

    return (
        <article className={`flex ${flexDirection}  items-center justify-between p-32`}>
            <div className="basis-1/3">
                {tag}
                <h2 className="text-4xl mt-8 font-bold my-4">{headerText}</h2>
                <p className="mb-8 text-sm font-medium">{paragraphText}</p>
                {button}
            </div>
            <div className={`flex-1 flex ${justifyDirection}`}>
                <Image src={imageSrc} alt={headerText} width={425} height={550} className="object-contain" />
            </div>
            {structuredData && (
                <Script id="content-ld" type="application/ld+json">{JSON.stringify(structuredData)}</Script>
            )}
        </article>
    );
};

export default Content;