import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/jest-globals';
import { describe, expect } from '@jest/globals';
import Button from './Button';

describe('Button Component', () => {
    it('renders the button with the correct text and href', () => {
        const buttonText = 'Click me';
        const buttonHref = '/test';
        render(<Button href={buttonHref}>{buttonText}</Button>);

        const buttonElement = screen.getByRole('link', { name: buttonText });
        expect(buttonElement).toHaveAttribute('href', buttonHref);
        expect(buttonElement).toHaveTextContent(buttonText);
    });

    it('applies additional class names when provided', () => {
        const additionalClass = 'bg-red-500';
        render(<Button href="/test" className={additionalClass}>Button</Button>);

        const buttonElement = screen.getByRole('link', { name: 'Button' });
        expect(buttonElement).toHaveClass(additionalClass);
    });
});
