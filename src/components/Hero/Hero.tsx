import React from 'react';
import Image from 'next/legacy/image';
import Script from "next/script";

type HeroProps = {
    logoSrc: string;
    logoSrcAlt: string;
    text: JSX.Element | JSX.Element[];
    structuredData?: Record<string, any>;
}

/**
 * Represents a Hero component.
 * @component
 * @param {object} props - The props object.
 * @param {string} props.logoSrc - The source URL of the logo image.
 * @param {string} props.logoSrcAlt - The alternate text for the logo image.
 * @param {string} props.text - The text displayed in the hero heading.
 * @param {object} props.structuredData - The structured data object.
 * @returns {ReactNode} The rendered Hero component.
 */
const Hero: React.FC<HeroProps> = ({ logoSrc,logoSrcAlt, text, structuredData }) => {
    return (
        <div data-testid="hero" className="flex flex-col items-center justify-center mb-2 lg:mb-16" role="banner">
            <div className="text-center mb-2 lg:mb-8">
                <div className="flex justify-center">
                    <Image src={logoSrc} alt={logoSrcAlt} width={176} height={108} objectFit="contain" />
                </div>
            </div>
            <div style={{maxWidth: '660px'}} className="mx-auto" aria-labelledby="hero-heading">
                <h1 id="hero-heading" className="text-white leading-5 lg:leading-hero-lh text-center text-hero font-semibold" style={{
                    lineHeight: '66px',
                }}>{text}</h1>
            </div>
            {structuredData && (
                <Script id="hero-ld" type="application/ld+json">{JSON.stringify(structuredData)}</Script>
            )}
        </div>
    );
};

export default Hero;