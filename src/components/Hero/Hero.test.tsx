import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/jest-globals';
import { describe, expect } from '@jest/globals';
import Hero from './Hero';

describe('Hero Component', () => {

    const heroText = (
        <>
            Own your <span className={'text-vq-blue'}>future</span> simple and for <span
            className={'text-vq-blue'}>free</span>
        </>
    );

    const mockProps = {
        logoSrc: '/mock_logo.png',
        logoSrcAlt: 'Mock Logo',
        text: heroText,
        structuredData: {
            "@context": "http://schema.org",
            "@type": "Organization",
            "name": "Mock Organization",
            "url": "http://example.com",
            "logo": "mock_logo.png"
        }
    };

    it('renders with no issues or errors', () => {
        render(<Hero {...mockProps} />);
    });

    it('displays correct hero text', () => {
        const { container } = render(<Hero {...mockProps} />);
        const spanElement = container.querySelector('span.text-vq-blue');
        expect(spanElement).toBeInTheDocument();
        expect(spanElement).toHaveTextContent("future");
    });
});
