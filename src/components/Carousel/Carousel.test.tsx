import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import '@testing-library/jest-dom/jest-globals';
import { describe, expect } from '@jest/globals';
import Carousel from './Carousel';

const images = [
    { src: 'image1.jpg', width: 300, height: 200 },
    { src: 'image2.jpg', width: 300, height: 200 },
    { src: 'image3.jpg', width: 300, height: 200 }
];

describe('Carousel Component', () => {
    it('renders with no issues or errors', () => {
        render(<Carousel images={images} />);
    });

    it('displays all images in the carousel', () => {
        const { getAllByAltText } = render(<Carousel images={images} />);
        const carouselImages = getAllByAltText(/Slide \d/);
        expect(carouselImages.length).toBe(images.length * 2);
    });
});