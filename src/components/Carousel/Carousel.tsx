'use client'
import React, {useEffect, useRef} from 'react';

interface CarouselProps {
    images: { src: string; width: number; height: number }[];
}

/**
 * Represents a carousel component.
 * @component
 * @param {Object} props - The props object.
 * @param {Array} props.images - The array of image objects to be displayed in the carousel.
 * @returns {JSX.Element} - The rendered carousel component.
 */
const Carousel: React.FC<CarouselProps> = ({images}) => {
    const carouselRef = useRef<HTMLDivElement>(null);

    useEffect(() => {
        const totalWidth = images[0].width * images.length;
        const singleSlideWidth = images[0].width * 4;
        const animationDuration = (totalWidth / singleSlideWidth) * 10;

        if (carouselRef.current) {
            carouselRef.current.style.animationDuration = `${animationDuration}s`;
        }
    }, [images]);

    const getDeterministicTopMargin = (index: number) => {
        const variations = [50, 100, 150, 200];
        return variations[index % variations.length];
    };

    return (
        <div data-testid="carousel" aria-label="Photo Gallery" role="region" aria-live="polite" className="overflow-hidden w-full relative">
            <div
                ref={carouselRef}
                className="flex animate-slide whitespace-nowrap"
                style={{
                    animationTimingFunction: 'linear',
                    animationIterationCount: 'infinite',
                }}
            >
                {images.concat(images).map((image, index) => (
                    <div key={index} className="flex-none m-4"
                         style={{width: '24rem', height: 'auto', marginTop: `${getDeterministicTopMargin(index)}px`}}>

                        <img src={image.src} alt={`Slide ${index}`}
                             style={{width: '100%', height: 'auto', objectFit: 'cover'}}/>
                    </div>
                ))}
            </div>
        </div>
    );
};

export default Carousel;
